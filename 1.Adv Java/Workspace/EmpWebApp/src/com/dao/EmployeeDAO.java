package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.model.Employee;

public class EmployeeDAO {

	public Employee empLogin(String emailId, String password) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		if (con == null) {
			System.out.println("Cannot Establish the Connection to the Database...");
			return null;
		}
				
		try {
			String selectQry = "Select * from employee where emailId = ? and password = ?";
			
			pst = con.prepareStatement(selectQry);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				
				Employee emp = new Employee();
				
				emp.setEmpId(rs.getInt("empId"));
				emp.setEmpName(rs.getString(2));
				emp.setSalary(rs.getDouble(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));
				
				return emp;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
				
		return null;		
	}
	
	
	public List<Employee> getAllEmployees() {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		if (con == null) {
			System.out.println("Cannot Establish the Connection to the Database...");
			return null;
		}
				
		try {
			String selectQry = "Select * from employee";
			
			pst = con.prepareStatement(selectQry);
			rs = pst.executeQuery();
			
			List<Employee> empList = new ArrayList<Employee>();
			
			while (rs.next()) {
				
				Employee emp = new Employee();
				
				emp.setEmpId(rs.getInt("empId"));
				emp.setEmpName(rs.getString(2));
				emp.setSalary(rs.getDouble(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));
				
				empList.add(emp);
			}
			
			return empList;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
				
		return null;		
	}
	
//	public int empRegister(String empName,double salary,String gender,String emailId,String password){
//		Connection con = DbConnection.getConnection();
//		PreparedStatement pst = null;
//		
//		if(con == null){
//	    	System.out.println("Cannot establish connection with the database");
//		}
//		
//		
//		try {
//			String insertQry = "insert into employee (empName,salary,gender,emailId,password) values(?,?,?,?,?)";
//			pst  = con.prepareStatement(insertQry);
//			pst.setString(1,empName);
//			pst.setDouble(2,salary);
//			pst.setString(3,gender);
//			pst.setString(4,emailId);
//			pst.setString(5,password);
//			
//			int result = pst.executeUpdate();
//			return result;
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		finally{
//			if(con != null){
//				try {
//					pst.close();
//					con.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		return 0;
//	}


public int registerEmployee(Employee emp) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		if (con == null) {
			System.out.println("Cannot Establish the Connection to the Database...");
			return 0;
		}
		
		try {
			String qry = "insert into employee(empName, salary, gender, emailId, password) values (?, ?, ?, ?, ?)";
			
			pst = con.prepareStatement(qry);
			
			pst.setString(1, emp.getEmpName());
			pst.setDouble(2, emp.getSalary());
			pst.setString(3, emp.getGender());
			pst.setString(4, emp.getEmailId());
			pst.setString(5, emp.getPassword());
			
			int result = pst.executeUpdate();
			
			return result;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return 0;
	}

public Employee getEmployeeById(int empId) {
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	if (con == null) {
		System.out.println("Cannot Establish the Connection to the Database...");
		return null;
	}
			
	try {
		String qry = "Select * from employee where empId = ?";
		
		pst = con.prepareStatement(qry);
		pst.setInt(1, empId);
		rs = pst.executeQuery();
		
		if (rs.next()) {
			
			Employee emp = new Employee();
			
			emp.setEmpId(rs.getInt(1));
			emp.setEmpName(rs.getString(2));
			emp.setSalary(rs.getDouble(3));
			emp.setGender(rs.getString(4));
			emp.setEmailId(rs.getString(5));
			emp.setPassword(rs.getString(6));
			
			return emp;
		}
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				rs.close();
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
			
	return null;		
}

	}
	