package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDAO;
import com.model.Employee;


@WebServlet("/Register")
public class Register extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");		
		PrintWriter out = response.getWriter();

		String empName = request.getParameter("empName");
		double salary = Double.parseDouble(request.getParameter("salary"));
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		Employee emp = new Employee(0, empName, salary, gender, emailId, password);
		
		EmployeeDAO empDAO = new EmployeeDAO();
		int result = empDAO.registerEmployee(emp);
		
		out.print("<html>");
		
		if (result > 0) {
			out.print("<body bgcolor='lightyellow' text='blue'>");
			out.print("<center><h1>Employee Registered Successfully!!!</h1>");	
			
			RequestDispatcher rd = request.getRequestDispatcher("Login.html");
			rd.include(request, response);		
			
			out.print("</center>");
		} else {
			out.print("<body bgcolor='lightyellow' text='red'>");
			out.print("<center><h1>Employee Registration Failed!!!</h1>");
			
			RequestDispatcher rd = request.getRequestDispatcher("Register.html");
			rd.include(request, response);	
			
			out.print("</center>");
		}
		out.print("</body>");
		out.print("</html>");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}