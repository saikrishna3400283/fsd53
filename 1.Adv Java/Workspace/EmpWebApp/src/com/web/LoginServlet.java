package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.EmployeeDAO;
import com.model.Employee;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
                //Add the following lines to implement the session in our project
		HttpSession session = request.getSession(true);
		session.setAttribute("emailId", emailId);
		

		out.print("<html>");
		if (emailId.equalsIgnoreCase("HR") && password.equalsIgnoreCase("HR")) {
		    //Update with HRHomePage.jsp from HRHomePage(Servlet)
		    RequestDispatcher rd = request.getRequestDispatcher("HrHomePage.jsp");
		    rd.forward(request, response);
		} else {

			EmployeeDAO empDAO = new EmployeeDAO();
			Employee emp = empDAO.empLogin(emailId, password);
			

			if (emp != null) {
				session.setAttribute("emp", emp);
				RequestDispatcher rd = request.getRequestDispatcher("EmpHomePage.jsp");
				rd.forward(request, response);
			} else {

				out.print("<body bgcolor=lightyellow text=red>");
				out.print("<center>");
				out.print("<h1>Invalid Credentials</h1>");

				RequestDispatcher rd = request.getRequestDispatcher("Login.html");
				rd.include(request, response);
			}
		}
		out.print("</center></body></html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}