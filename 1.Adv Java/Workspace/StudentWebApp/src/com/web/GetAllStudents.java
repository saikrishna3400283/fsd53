package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;
import com.model.Student;

@WebServlet("/GetAllStudents")
public class GetAllStudents extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		StudentDAO stuDAO = new StudentDAO();
		List<Student> stuList = stuDAO.getAllStudents();
		
		RequestDispatcher rd = request.getRequestDispatcher("AdminHome");
		rd.include(request, response);
		
		if(stuList != null){
			out.print("<center>");
			
			out.print("<table border=2>");
			out.print("<thead>");
			out.print("<tr>");
			out.print("<th>Student Id</th>");
			out.print("<th>Student Name</th>");
			out.print("<th>Email</th>");
			out.print("<th>Gender</th>");
			out.print("<th>Password</th>");
			out.print("<thead>");
			out.print("<tr>");
			
			
			for(Student stu : stuList){
				out.print("<tr>");
				out.print("<td>"+ stu.getStudentId() +"</td>");
				out.print("<td>"+ stu.getStudentName() +"</td>");
				out.print("<td>"+ stu.getEmailId() +"</td>");
				out.print("<td>"+ stu.getGender() +"</td>");
				out.print("<td>"+ stu.getPassword() +"</td>");
				out.print("</tr>");
			}
			out.print("</table>");
			out.print("</center>");
		}
		else{
			out.print("<center><h1 style='color:red;'>Record(s) Not Found..!!</h1></center>");
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
