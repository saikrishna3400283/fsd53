package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;
import com.model.Student;


@WebServlet("/GetStudentById")
public class GetStudentById extends HttpServlet {

	private int studentId;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int studentId = Integer.parseInt(request.getParameter("studentId"));
		
		StudentDAO stuDAO = new StudentDAO();
		Student stu = stuDAO.getStudentById(studentId);
		
//		out.print("<a href='Profile'>Profile</a> &nbsp; &nbsp; &nbsp;");
		
		RequestDispatcher rd = request.getRequestDispatcher("AdminHome");
		rd.include(request, response);
		
		if (stu != null) {
			out.print("<br/>");
			out.print("<center>");
			
			out.print("<table border=2>");
			out.print("<thead>");
			out.print("<tr>");
			out.print("<th>StudentId</th>");
			out.print("<th>StudentName</th>");
			out.print("<th>Gender</th>");
			out.print("<th>EmailId</th>");
			out.print("<th>Password</th>");
			out.print("</tr>");
			out.print("</thead>");
			
			out.print("<tr>");
			out.print("<td>" + stu.getStudentId()   + "</td>");
			out.print("<td>" + stu.getStudentName() + "</td>");
			out.print("<td>" + stu.getGender()  + "</td>");
			out.print("<td>" + stu.getEmailId()  + "</td>");
			out.print("<td>" + stu.getPassword() + "</td>");
			out.print("</tr>");
			
			out.print("</table>");			
			out.print("</center>");	
		} else {
			out.print("<center>");	
			out.print("<br/><h3 style='color:red;'>Student Record Not Found!!!</h3>");
			out.print("</center>");	
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
